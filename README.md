# Task Tracker #

### What can this application do? ###

* Create personal tasks
* Demonstrating all the tasks that user has
* Create sub-tasks for exiting tasks
* Edit and remove tasks

### How to install ? ###

* $ python install setup.py

### How to run? ###

* $ track -h

Made by Artem Denisov.

Group 653502.

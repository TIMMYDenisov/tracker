#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from todo_tracker.reminder_manager.reminder_operations.show import show_notification
from todo_tracker.reminder_manager.reminder_manager import ReminderManager
from todo_tracker.task_manager.tasks_operations.show import show_info
from todo_tracker.task_manager.task_manager import TaskManager
from todo_tracker.user_manager.user_manager import UserManager
from todo_tracker.reminder.reminder import Reminder
from todo_tracker.config.config import load_config
from todo_tracker.parser.parser import TrackParse
from todo_tracker.storage.storage import Storage
from todo_tracker.exceptions.exceptions import TaskError, UserError, ConfigError
from todo_tracker.tasks.task import Task
from todo_tracker.user.user import User
import logging


def main():
    args = TrackParse()
    config = load_config()
    try:
        info_storage = Storage(config.get('Paths', 'tasks_path'), config.get('Paths', 'users_path'),
                               config.get('Paths', 'reminds_path'))
        task_manager = TaskManager(info_storage)
        reminder_manager = ReminderManager(info_storage)
        user_manager = UserManager(info_storage)
        user = User(args.login, args.password, args.name)
        if args.user_flag:
            user_manager.create_new_user(user.login, user.password, user.name)
        elif user_manager.authorization(user.login, user.password):

                reminder_manager.update_statuses(user.login)
                if args.task_action == 'create':
                    task = Task(args.content, args.priority, args.group, args.execution_date, user.login)
                    reminder = Reminder(args.frequency, user.login, args.week_day)
                    task_manager.create_new_task(task.task_owner, args.sub_flag, args.hid, task.content, task.priority,
                                                 task.group, task.execution_date)
                    reminder_manager.create_new_reminder(args.hid, reminder.owner, reminder.week_day, args.sub_flag,
                                                         reminder.frequency)
                    show_notification(reminder_manager.get_notification_for_today(user.login))
                elif args.task_action == 'remove':
                    task_manager.remove_task(user.login, args.sub_flag, args.sid)
                    reminder_manager.remove_reminder(args.sub_flag, args.hid, args.sid, user.login)
                    show_notification(reminder_manager.get_notification_for_today(user.login))
                elif args.task_action == 'edit':
                    task_manager.edit_task(user.login, args.sub_flag, args.sid, args.content,
                                           args.priority, args.group, args.execution_date, args.status)
                    show_notification(reminder_manager.get_notification_for_today(user.login))
                elif args.task_action == 'show':
                    show_notification(reminder_manager.get_notification_for_today(user.login))
                    show_info(task_manager.get_tasks_by_query(user.login, args.query, args.sub_flag, args.sid))

                else:
                    raise TaskError('Wrong entry for task action!')
    except TaskError as te:
        logging.info(te.msg)
        exit(1)
    except UserError as ue:
        logging.info(ue.msg)
        exit(2)
    except ConfigError as ce:
        logging.info(ce.msg)
        exit(3)


if __name__ == "__main__":
    main()

#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from datetime import datetime
import os


def date_check(value):
    current_date = datetime.strptime(datetime.now().strftime('%d.%m.%Y %H:%M'), '%d.%m.%Y %H:%M')
    execution_date = datetime.strptime(value, '%d.%m.%Y %H:%M')
    if (current_date - execution_date).total_seconds() >= 0:
        return None
    else:
        return execution_date


def path_check(path):
    if os.path.exists(path):
        return True
    else:
        return False


def status_check(user_id, storage):
    current_date = datetime.strptime(datetime.now().strftime('%d.%m.%Y %H:%M'), '%d.%m.%Y %H:%M')
    tasks = storage.load_user_tasks(False, user_id)
    for task in tasks:
        execution_date = datetime.strptime(task.get('execution_date'), '%d.%m.%Y %H:%M')
        if (current_date - execution_date).total_seconds() >= 0 and task.get('status') != 'Done':
            task.update({'status': 'Failed'})
            recursive_update_sub_status(storage, user_id, task.get('id'), task.get('status'))
            storage.update_existed_task(False, task)
        elif task.get('status') == 'Done':
            recursive_update_sub_status(storage, user_id, task.get('id'), task.get('status'))
        elif task.get('status') == 'Failed':
            recursive_update_sub_status(storage, user_id, task.get('id'), task.get('status'))
        else:
            task.update({'status': 'In progress'})
            recursive_update_sub_status(storage, user_id, task.get('id'), task.get('status'))
            storage.update_existed_task(False, task)


def recursive_update_sub_status(storage, user_id, parent_id, parent_status):
    current_date = datetime.strptime(datetime.now().strftime('%d.%m.%Y %H:%M'), '%d.%m.%Y %H:%M')
    subs = storage.load_user_tasks(parent_id, user_id)
    for sub in subs:
        if sub.get('parent_id') == parent_id:
            execution_date = datetime.strptime(sub.get('execution_date'), '%d.%m.%Y %H:%M')
            if (current_date - execution_date).total_seconds() >= 0 and sub.get('status') != 'Done':
                sub.update({'status': 'Failed'})
                storage.update_existed_task(True, sub)
            elif sub.get('status') in ['Failed', 'Done']:
                continue
            else:
                sub.update({'status': parent_status})
                storage.update_existed_task(True, sub)


def is_active_status(sub_flag, storage, task_id, user_id):
    task = storage.load_task_by_id(user_id, sub_flag, task_id)
    if task.get('status') in ['Failed', 'Done']:
        return False
    return True


def is_execution_today(sub_flag, storage, task_id, user_id):
    task = storage.load_task_by_id(user_id, sub_flag, task_id)
    execution_date = datetime.strptime(task.get('execution_date'), '%d.%m.%Y %H:%M')
    if execution_date.date() == datetime.today().date():
        return True
    return False

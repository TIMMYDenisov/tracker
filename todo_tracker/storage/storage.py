#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from todo_tracker.checkers.task_prop_check import path_check
from todo_tracker.exceptions import exceptions
import logging
import json
import os


class Storage:
    def __init__(self, task_path, user_path, r_path):
        self.t_path = self.path_set(task_path)
        self.us_path = self.path_set(user_path)
        self.r_path = self.path_set(r_path)

    @staticmethod
    def path_set(value):
        if path_check(value):
            return value
        else:
            raise exceptions.ConfigError('Wrong path')

    def load_user_by_id(self, user_id):
        with open(os.path.join(self.us_path, 'users.json'), 'r') as outfile:
            users = json.load(outfile)
        if user_id in users:
            return {user_id: users.get(user_id)}
        else:
            return False

    def save_user(self, content):
        with open(os.path.join(self.us_path, 'users.json'), 'r') as outfile:
            all_users = json.load(outfile)
        all_users.update(content)
        with open(os.path.join(self.us_path, 'users.json'), 'w') as outfile:
            json.dump(all_users, outfile, indent=2, sort_keys=True)

    def load_user_tasks(self, sub_flag, user_id):
        if sub_flag:
            with open(os.path.join(self.t_path, 'sub_tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        else:
            with open(os.path.join(self.t_path, 'tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        out_list = list()
        for task in tasks:
            if task.get('owner') == user_id:
                out_list.append(task)
        return out_list

    def save_tasks(self, sub_flag, content):
        if not sub_flag:
            with open(os.path.join(self.t_path, 'tasks.json'), 'w') as outfile:
                json.dump(content, outfile, indent=2, sort_keys=True)
        else:
            with open(os.path.join(self.t_path, 'sub_tasks.json'), 'w') as outfile:
                json.dump(content, outfile, indent=2, sort_keys=True)

    def load_user_reminds(self, user_id):
        with open(os.path.join(self.r_path, 'reminder.json'), 'r') as outfile:
            all_reminders = json.load(outfile)
        out_list = list()
        for reminder in all_reminders:
            if reminder.get('owner') == user_id:
                out_list.append(reminder)
        return out_list

    def save_reminds(self, content):
        with open(os.path.join(self.r_path, 'reminder.json'), 'w') as outfile:
            json.dump(content, outfile, indent=2, sort_keys=True)

    def add_new_remind(self, item):
        with open(os.path.join(self.r_path, 'reminder.json'), 'r') as outfile:
            all_reminders = json.load(outfile)
        if not all_reminders:
            rem_id = 1
        else:
            rem_id = all_reminders[-1].get('id') + 1
        item.update({'id': rem_id})
        all_reminders.append(item)
        self.save_reminds(all_reminders)

    def load_task_reminder(self, task_id, parent_id, user_id):

        with open(os.path.join(self.r_path, 'reminder.json'), 'r') as outfile:
            all_reminders = json.load(outfile)
        for i, reminder in enumerate(all_reminders):
            if reminder.get('task_id') == task_id and reminder.get('parent_id') == parent_id:
                if reminder.get('owner') == user_id:
                    return reminder
                else:
                    raise exceptions.UserError('Access denied. You do not own this task')
        return None

    def pop_reminder(self, reminder_id):
        with open(os.path.join(self.r_path, 'reminder.json'), 'r') as outfile:
            all_reminders = json.load(outfile)
        all_reminders.remove(reminder_id)
        self.save_reminds(all_reminders)

    def update_existed_reminder(self, item):
        with open(os.path.join(self.r_path, 'reminder.json'), 'r') as outfile:
            all_reminders = json.load(outfile)
        for i, remind in enumerate(all_reminders):
            if remind.get('id') == item.get('id'):
                all_reminders[i].update(item)
        self.save_reminds(all_reminders)

    def add_new_task(self, sub_flag, item):
        if sub_flag:
            with open(os.path.join(self.t_path, 'sub_tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        else:
            with open(os.path.join(self.t_path, 'tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        if not tasks:
            task_id = 1
        else:
            task_id = tasks[-1].get('id') + 1
        item.update({'id': task_id})
        tasks.append(item)
        self.save_tasks(sub_flag, tasks)

    def pop_task(self, sub_flag, task_id):
        if sub_flag:
            with open(os.path.join(self.t_path, 'sub_tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        else:
            with open(os.path.join(self.t_path, 'tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        tasks.remove(task_id)
        self.save_tasks(sub_flag, tasks)

    def update_existed_task(self, sub_flag, item):
        if sub_flag:
            with open(os.path.join(self.t_path, 'sub_tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        else:
            with open(os.path.join(self.t_path, 'tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        for i, task in enumerate(tasks):
            if task.get('id') == item.get('id'):
                tasks[i].update(item)
        self.save_tasks(sub_flag, tasks)

    def load_task_by_id(self, user_id, sub_flag, task_id):
        if sub_flag:
            with open(os.path.join(self.t_path, 'sub_tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        else:
            with open(os.path.join(self.t_path, 'tasks.json'), 'r') as outfile:
                tasks = json.load(outfile)
        for task in tasks:
            if task.get('id') == task_id:
                if task.get('owner') == user_id:
                    return task
                else:
                    raise exceptions.UserError('Access denied. You do not own this task')
        return None

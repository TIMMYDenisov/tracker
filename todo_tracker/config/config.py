#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from todo_tracker.config.logger import logger
import configparser
import json
import os


def load_config():
    config = __load_config_file()
    logs_path = config.get('Paths', 'logs_path')
    logger.init_logger(logs_path)
    return config


def __load_config_file():
    config = configparser.ConfigParser()
    default_config_path = os.path.expanduser('~/.track/config/config.ini')
    if os.path.exists(default_config_path):
        config.read(default_config_path)
        return config
    else:
        return __create_default_config()


def __create_default_config():
    default_dir = os.path.expanduser('~/.track')
    default_user_dir = os.path.join(default_dir, 'users')
    default_cfg_path = os.path.join(default_dir, 'config')
    default_tasks_path = os.path.join(default_dir, 'tasks')
    default_logs_path = os.path.join(default_dir, 'logs')
    default_reminds_path = os.path.join(default_dir, 'reminder')

    def create_main_config():
        config = configparser.ConfigParser()
        config.add_section('Paths')
        config.set('Paths', 'workspace_path', default_dir)
        config.set('Paths', 'users_path', default_user_dir)
        config.set('Paths', 'tasks_path', default_tasks_path)
        config.set('Paths', 'logs_path', default_logs_path)
        config.set('Paths', 'reminds_path', default_reminds_path)
        with open(os.path.join(default_cfg_path, 'config.ini'), 'w') as cfg_file:
            config.write(cfg_file)
        return config

    def create_json_config():
        config = {'Path': {
                'workspace_path': default_dir,
                'users_path': default_user_dir,
                'tasks_path': default_tasks_path,
                'logs_path': default_logs_path,
                'reminds_path': default_reminds_path
            }
        }
        with open(os.path.join(default_cfg_path, 'config.json'), 'w') as j_conf:
            json.dump(config, j_conf, indent=2, sort_keys=True)

    if not os.path.exists(default_dir):
        os.mkdir(default_dir)
    if not os.path.exists(default_user_dir):
        os.mkdir(default_user_dir)
        content = {}
        with open(os.path.join(default_user_dir, 'users.json'), 'w') as j_file:
            json.dump(content, j_file)
    if not os.path.exists(default_reminds_path):
        os.mkdir(default_reminds_path)
        content = list()
        with open(os.path.join(default_reminds_path, 'reminder.json'), 'w') as j_file:
            json.dump(content, j_file)
    if not os.path.exists(default_tasks_path):
        os.mkdir(default_tasks_path)
        content = list()
        with open(os.path.join(default_tasks_path, 'tasks.json'), 'w') as j_file:
            json.dump(content, j_file)
        with open(os.path.join(default_tasks_path, 'sub_tasks.json'), 'w') as j_file:
            json.dump(content, j_file)
    if not os.path.exists(default_logs_path):
        os.mkdir(default_logs_path)
    if not os.path.exists(default_cfg_path):
        os.mkdir(default_cfg_path)
    create_json_config()
    return create_main_config()

#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from datetime import date
import logging
import os


def init_logger(logs_dir):
    log_path = os.path.join(logs_dir, str(date.today())) + '.txt'
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handl = logging.FileHandler(log_path)
    file_handl.setLevel(logging.DEBUG)
    file_handl.setFormatter(formatter)
    logger.addHandler(file_handl)

    formatter = logging.Formatter('%(message)s')
    console_handl = logging.StreamHandler()
    console_handl.setLevel(logging.INFO)
    console_handl.setFormatter(formatter)
    logger.addHandler(console_handl)

#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from todo_tracker.exceptions import exceptions
import logging


class User:
    def __init__(self, login, password, name):
        self.login = login
        self.password = password
        self.name = name

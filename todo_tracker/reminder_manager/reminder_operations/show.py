#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


def show_notification(info):
    for key, value in info.items():
        if type(value) != list():
            print(key.format(value))
        else:
            print(key.format(', '.join([str(i) for i in value])))

#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from datetime import datetime
import calendar


class Reminder:
    def __init__(self, frequency, owner, week_day):
        self.owner = owner
        self.week_day = None
        self.frequency = frequency

        self.week_day_validation(frequency, week_day)

    def week_day_validation(self, frequency, week_day):
        if frequency == 'WR':
            self.week_day = None
        elif frequency == 'ED':
            self.week_day = list(calendar.day_abbr)
        else:
            if week_day:
                self.week_day = week_day
            else:
                self.week_day = calendar.day_abbr[datetime.today().weekday()]

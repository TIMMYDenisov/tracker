#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from todo_tracker.exceptions import exceptions
import logging


def show_info(tasks):
    if tasks:
        for task in tasks:
            if task.get('parent_id') is None:
                for key, value in sorted(task.items()):
                    if task.get('status') == 'In progress':
                        print('\x1b[5;30;42m' + '{} : {}'.format(key, value) + '\x1b[0m')
                    if task.get('status') == 'Done':
                        print('\x1b[0;30;44m' + '{} : {}'.format(key, value) + '\x1b[0m')
                    if task.get('status') == 'Failed':
                        print('\x1b[5;30;41m' + '{} : {}'.format(key, value) + '\x1b[0m')
                print('\n')
            else:
                for key, value in sorted(task.items()):
                    if task.get('status') == 'In progress':
                        print('\t' + '\x1b[5;30;42m' + '{} : {}'.format(key, value) + '\x1b[0m')
                    if task.get('status') == 'Done':
                        print('\t' + '\x1b[0;30;44m' + '{} : {}'.format(key, value) + '\x1b[0m')
                    if task.get('status') == 'Failed':
                        print('\t' + '\x1b[5;30;41m' + '{} : {}'.format(key, value) + '\x1b[0m')
    else:
        raise exceptions.TaskError('You have not have any tasks')
